import { AuthConfig } from 'angular-oauth2-oidc';

export const authCodeFlowConfig: AuthConfig = {
  // Url of the Identity Provider
  //issuer: 'https://idsvr4.azurewebsites.net',
  issuer: 'https://id.tris.vn',
  // URL of the SPA to redirect the user to after login
  redirectUri: window.location.origin,
  clientId: 'postman',
  responseType: 'code',
  scope: 'openid profile email roles',
  oidc: true,
  showDebugInformation: true,
};
